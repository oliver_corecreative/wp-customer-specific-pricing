<?php

namespace cspFunctions;

if (! class_exists('WdmWuspFunctions')) {

    class WdmWuspFunctions
    {
        private static $instance;
        /**
         * Returns the *Singleton* instance of this class.
         *
         * @return Singleton The *Singleton* instance.
         */
        public static function getInstance()
        {
            if (null === static::$instance) {
                static::$instance = new static();
            }

            return static::$instance;
        }

        /**
        * Save customer specific pricing pair from rules.
        * @param array $customer_ids selected customers
        * @param array $product_values Product names.
        * @param array $product_quantities Product quantities.
        * @param array $product_actions Price-type selected.
        * @param string $query_title rule title.
        * @param string $option_name rule-type.
        * @param int $current_query_id current rule id.
        * @return string HTML for the message on Rules Page.
        */
        public function saveCustomerPricingPair($customer_ids, $product_values, $product_quantities, $oldQuantities, $product_actions, $query_title, $option_name, $current_query_id = null)
        {
            global $wpdb;
            $selection_details[ 'table_name' ]       = $wpdb->prefix . 'wusp_user_pricing_mapping';
            $selection_details[ 'selection_column' ] = 'user_id';
            $selection_details[ 'selection_type' ]   = 'customer';

            return $this->saveSelectionPairs($selection_details, $customer_ids, $product_values, $product_quantities, $oldQuantities, $product_actions, $query_title, $option_name, $current_query_id);
        }

//function ends -- saveCustomerPricingPair

        /**
        * Save Role specific pricing pair from rules.
        * @param array $role_list selected roles
        * @param array $product_values Product names.
        * @param array $product_quantities Product quantities.
        * @param array $product_actions Price-type selected.
        * @param string $query_title rule title.
        * @param string $option_name rule-type.
        * @param int $current_query_id current rule id.
        * @return string HTML for the message on Rules Page.
        */
        public function saveRolePricingPair($role_list, $product_values, $product_quantities, $oldQuantities, $product_actions, $query_title, $option_name, $current_query_id = null)
        {
            global $wpdb;

            $selection_details[ 'table_name' ]       = $wpdb->prefix . 'wusp_role_pricing_mapping';
            $selection_details[ 'selection_column' ] = 'role';
            $selection_details[ 'selection_type' ]   = 'role';

            return $this->saveSelectionPairs($selection_details, $role_list, $product_values, $product_quantities, $oldQuantities, $product_actions, $query_title, $option_name, $current_query_id);
        }

//function ends -- saveRolePricingPair

        /**
        * Save Group specific pricing pair from rules.
        * @param array $group_ids selected groups.
        * @param array $product_values Product names.
        * @param array $product_quantities Product quantities.
        * @param array $product_actions Price-type selected.
        * @param string $query_title rule title.
        * @param string $option_name rule-type.
        * @param int $current_query_id current rule id.
        * @return string HTML for the message on Rules Page.
        */
        public function saveGroupPricingPair($group_ids, $product_values, $product_quantities, $oldQuantities, $product_actions, $query_title, $option_name, $current_query_id = null)
        {
            global $wpdb;

            $selection_details[ 'table_name' ]       = $wpdb->prefix . 'wusp_group_product_price_mapping';
            $selection_details[ 'selection_column' ] = 'group_id';
            $selection_details[ 'selection_type' ]   = 'group';

            return $this->saveSelectionPairs($selection_details, $group_ids, $product_values, $product_quantities, $oldQuantities, $product_actions, $query_title, $option_name, $current_query_id);
        }

//function ends -- saveGroupPricingPair
        /**
        * Checks if Pricing row exists.
        * @param string $table_name table name.
        * @param string $option_type rule-type.
        * @param string $option_key column name.
        * @return int $result count of the entries.
        */
        private static function checkPricingRow($table_name, $option_type, $option_key)
        {
            global $wpdb;

            $query   = "Select Count(`id`) from `" . $table_name . "` where " . $option_key . " = '" . $option_type . "'";
            $result  = $wpdb->get_var($query);
            return $result;
        }

        /**
         * Sort a 2 dimensional array based on 1 or more indexes.
         *
         * msort() can be used to sort a rowset like array on one or more
         * 'headers' (keys in the 2th array).
         *
         * @param array        $array      The array to sort.
         * @param string|array $key        The index(es) to sort the array on.
         * @param int          $sort_flags The optional parameter to modify the sorting
         *                                 behavior. This parameter does not work when
         *                                 supplying an array in the $key parameter.
         *
         * @return array The sorted array.
         */
        public function msort($array, $key)
        {
            if (is_array($array) && count($array) > 0) {
                if (!empty($key)) {
                    $mapping = array();
                    foreach ($array as $k => $v) {
                        $sort_key = '';
                        if (!is_array($key)) {
                            $sort_key = $v[$key];
                        }
                        $mapping[$k] = $sort_key;
                    }
                    asort($mapping, SORT_REGULAR);
                    $sorted = array();
                    foreach ($mapping as $k => $v) {
                        $sorted[$k] = $array[$k];
                    }
                    return $sorted;
                }
            }
            return $array;
        }

        /**
        * Returns true if quantity present in array.
        * @param array $array array of quantities-pricing for any entity.
        * @param int $qty each value in quantities array.
        * @return bool true if present otherwise false.
        */
        public function hasQty($array, $qty)
        {
            $qtyArray = $this->getArrayColumn($array, 'min_qty');

            if (count($qtyArray) > 0 && in_array($qty, $qtyArray)) {
                return true;
            }
            return false;
        }
        /**
        * Returns true if Quantity is there in quantity pricing array.
        * @param array $qtysArray MinQuantities array
        * @param int $qty Quantity of Product.
        * @return true if present in array
        */
        public function hasQtyInPriceArray($qtysArray, $qty)
        {
            if (count($qtysArray) > 0 && in_array($qty, $qtysArray)) {
                return true;
            }
            return false;
        }

        /**
        * If category specific pricing is there for particular product.
        * More Priority given to user/group/role specific pricing than
        * category specific pricing.
        * @param array $productPrices entity specific pricing details.
        * @param array $catPrices category specific pricing for entity.
        * @return array $allPrices merged specific pricing for that entity.
        */
        public function mergeProductCatPriceSearch($productPrices, $catPrices)
        {
            $allPrices = array();
            foreach ($productPrices as $key => $record) {
                if (isset($catPrices[$record['product_id']]) && isset($catPrices[$record['product_id']][$record['min_qty']])) {
                    continue;
                } else {
                    $allPrices[] = $record;
                }
            }
            
            foreach ($catPrices as $key => $record) {
                foreach ($record as $value) {
                    $allPrices[] = $value;
                }
            }
            return $allPrices;
        }

        /**
        * Gets the Product categories for specific product.
        * @param object $product wc-product
        * @return array array of the categories of that product.
        */
        public function getProductCategories($product)
        {
            $productId = $product->get_id();
            if ('simple' === $product->get_type()) {
                $productId = $product->get_id();
            }
       
            if ('variation' === $product->get_type()) {
                if (version_compare(WC_VERSION, '3.0', '<')) {
                    $productId = $product->id;
                } else {
                    $productId = $product->get_parent_id();
                }
            }
            return wp_get_post_terms($productId, 'product_cat');
        }

        /**
        * Save selected specific pricing pair from rules.
        * Add the rule in DB.
        * Gets the subrules for that rule.
        * Updates the rules and subrules based on recent pricing selection.
        * @param array $selection_details DB details for the entity of
        * selection.
        * @param array $selection_ids selected entities.
        * @param array $product_values Product names.
        * @param array $product_quantities Product quantities.
        * @param array $product_actions Price-type selected.
        * @param string $query_title rule title.
        * @param string $option_name rule-type.
        * @param int $current_query_id current rule id.
        * @return string HTML for the message on Rules Page.
        */
        private function saveSelectionPairs($selection_details, $selection_ids, $product_values, $product_quantities, $oldQuantities, $product_actions, $query_title, $option_name, $current_query_id = null)
        {
            global $ruleManager, $subruleManager;
            $ruleCreated     = true;
            $error                       = '';
            $subrulesOfRule              = array();
            //Create Main Rule
            $rule_id                     = $current_query_id;

            if (empty($current_query_id)) {
                $rule_id = $ruleManager->addRule($query_title, $selection_details[ 'selection_type' ]);
                //If main rule can not be created, Return here
                if ($rule_id === false) {
                    return;
                }
            } else {
                $subrulesOfRule = self::getSubruleOfRule($current_query_id, $rule_id, $query_title, $selection_details);
            }

            $selection_entry_list    = array();
            $price_list              = array();
            $product_id_list         = array();

            $row_count           = 0;
            $total_process_count = count($selection_ids) * count($product_values);

            $selection_entry_list = self::loopSelectionIds($selection_ids, $product_values, $product_quantities, $oldQuantities, $product_actions, $option_name, $row_count, $total_process_count, $selection_details, $selection_entry_list, $product_id_list, $rule_id, $price_list);

            delete_option($option_name . '_value');
            delete_option($option_name . '_status');

            if ($subruleManager->countSubrules($rule_id) == 0) {
                $ruleManager->deleteRule($rule_id);
                $error                   = __('Rule could not be created', CSP_TD);
                $ruleCreated = false;
            }
            $subruleErrors = trim($subruleManager->errors);
            $ruleErrors = trim($ruleManager->errors);
            if (!empty($subruleErrors) || !empty($ruleErrors)) {
                $ruleCreated = false;
            }

            if ($ruleCreated) {
                //Delete old subrules asssociated with current rule
                if (! empty($current_query_id)) {
                    $subruleManager->deleteSubrules($subrulesOfRule);
                }
                
                $ruleManager->updateTotalNumberOfSubrules($rule_id);
                $ruleManager->setUnusedRulesAsInactive();
            }
            return $this->sendSelectionResult($ruleCreated, $ruleManager->errors . ' ' . $subruleManager->errors . ' ' . $error, $selection_entry_list, $rule_id, $current_query_id);
        } //function ends -- saveSelectionPairs

        /**
        * Adds the selected product-pricings in the database.
        * Returns the array of the insertion ids of the pricing mappings.
        * @param array $selection_ids selected entities.
        * @param array $product_values Product names.
        * @param array $product_quantities Product quantities.
        * @param array $product_actions Price-type selected.
        * @param string $option_name rule-type.
        * @param int $row_count intially zero
        * @param int $total_process_count product of count of selections and * products
        * @param array $selection_details DB details for the entity of
        * selection.
        * @param array $selection_entry_list initially empty
        * @param array $product_id_list initially empty
        * @param int $rule_id Rule Id.
        * @param array $price_list initially empty
        * @param string $query_title rule title.

        * @param int $current_query_id current rule id.
        */
        private static function loopSelectionIds($selection_ids, $product_values, $product_quantities, $oldQuantities, $product_actions, $option_name, $row_count, $total_process_count, $selection_details, $selection_entry_list, $product_id_list, $rule_id, $price_list)
        {
            if (! empty($product_values) && ! empty($selection_ids)) {
                foreach ($product_values as $key => $price) {
                    $pattern = trim(str_replace('csp_value_', '', $key));
                    $temp=preg_match_all("/(\\d+)/is", $pattern, $matches);
                    $productId = $matches[1][0];
                    $userId = trim(str_replace("{$productId}_", '', $pattern));

                    $check_rows_exist = self::checkPricingRow($selection_details[ 'table_name' ], $userId, $selection_details[ 'selection_column' ]);
                    $selection_entry_list = self::loopProductValues($productId, $product_quantities, $oldQuantities, $product_actions, $check_rows_exist, $option_name, $row_count, $total_process_count, $selection_details, $userId, $selection_entry_list, $product_id_list, $rule_id, $price_list, $price);
                }//foreach ends
            }
            return $selection_entry_list;
        } //end of function loopSelectionIds

        /**
        * Gets the subrules of the rule.
        * Updates the rule with the details.
        * Gets the subrules associated with them.
        * @param int $current_query_id current rule id.
        * @param int $rule_id current rule id.
        * @param string $query_title rule title.
        * @param array $selection_details DB details for the entity of
        * selection.
        * @return array $subrules subrule ids array for particular rule id.
        */
        private static function getSubruleOfRule($current_query_id, $rule_id, $query_title, $selection_details)
        {
            global $ruleManager, $subruleManager;
            if (! is_numeric($current_query_id)) {
                return;
            }

            $ruleUpdateStatus    = $ruleManager->updateRule($rule_id, array(
                'rule_title' => $query_title,
                'rule_type' => $selection_details[ 'selection_type' ] ));
            $subrulesOfRule      = $subruleManager->getSubruleIds($rule_id);

            //If main rule can not be created, Return here
            if ($ruleUpdateStatus === false) {
                return;
            }
            return $subrulesOfRule;
        } //end of function getSubruleOfRule

        /**
        * Updates the progress status in options DB.
        * Updates the product-pricing in DB if already exists.
        * Add new entry in DB if new product-pricing.
        * Adds the subrule associated with the product-pricing.
        * @param int $productId Product Id.
        * @param array $selection_ids selected entities.
        * @param array $product_quantities Product quantities.
        * @param array $product_actions Price-type selected.
        * @param int $check_rows_exist count of pricing row.
        * @param string $option_name rule-type.
        * @param int $row_count intially zero
        * @param int $total_process_count product of count of selections and
        * products
        * @param array $selection_details DB details for the entity of
        * selection.
        * @param int $userId User Id.
        * @param array $selection_entry_list initially empty
        * @param array $product_id_list initially empty
        * @param int $rule_id Rule Id.
        * @param array $price_list initially empty
        * @param string $query_title rule title.
        * @param float $price Price for product.
        * @return array $selection_entry_list insertion ids of the product-pricing * in DB.
        */
        private static function loopProductValues($productId, $product_quantities, $oldQuantities, $product_actions, $check_rows_exist, $option_name, $row_count, $total_process_count, $selection_details, $userId, $selection_entry_list, $product_id_list, $rule_id, $price_list, $price)
        {
            global $wpdb, $subruleManager;

            self::updateProgressOption($row_count, $total_process_count, $option_name);
            $row_count = $row_count + 1;

            if ($price != '' && floatval($price) >= 0) {
                update_option($option_name . "_status", __('Processing Product ID ', CSP_TD) . ' ' . $productId);

                $flat_or_discount    = isset($product_actions[ 'wdm_csp_price_type' . $productId.'_'.$userId ]) ? ($product_actions[ 'wdm_csp_price_type' . $productId.'_'.$userId ] == 2 ? 2 : 1) : 1;

                $quantity = isset($product_quantities[ 'csp_qty_' . $productId.'_'.$userId ]) ? $product_quantities[ 'csp_qty_' . $productId.'_'.$userId ] : 1;
                $oldQuantity = isset($oldQuantities[ 'csp_qty_' . $productId.'_'.$userId ]) ? $oldQuantities[ 'csp_qty_' . $productId.'_'.$userId ] : 1;
                if ($check_rows_exist > 0) {
                    $check_exists        = "SELECT `id` FROM `" . $selection_details[ 'table_name' ] . "` WHERE `" . $selection_details[ 'selection_column' ] . "` = '" . $userId . "' AND `product_id` = '" . $productId . "' AND `min_qty` = '".$oldQuantity."'";
                    $row_exist_result    = $wpdb->get_results($check_exists);

                    if ($row_exist_result && isset($row_exist_result[ 0 ])) {
                        //Update the result
                        self::updateSelectionPair($selection_details[ 'selection_type' ], $row_exist_result[ 0 ]->id, $userId, $productId, $price, $flat_or_discount, $quantity);
                        $selection_entry_list[]  = intval($row_exist_result[ 0 ]->id);
                        $price_list[]            = $price;
                    } else {
                        //Insert the result
                        $selection_entry_list[]  = self::setSelectionPair($selection_details[ 'selection_type' ], $userId, $productId, $price, $flat_or_discount, $quantity);
                        $price_list[]            = $price;
                    }
                } else {
                    $selection_entry_list[]  = self::setSelectionPair($selection_details[ 'selection_type' ], $userId, $productId, $price, $flat_or_discount, $quantity);
                    $price_list[]            = $price;
                }
                $subruleManager->addSubrule($rule_id, $productId, $quantity, $flat_or_discount, $price, $selection_details[ 'selection_type' ], $userId);

                if (! in_array($productId, $product_id_list)) {
                    $product_id_list[] = $productId;
                }
            }//if ends -- Price not empty
            // }//foreach ends
            return $selection_entry_list;
        } //end processProductValues

        /**
        * Gets the column or the values of the keys sent.
        * For example, slugs for categories in array
        * @param array $array array of entity.
        * @param string $column column or value name.
        * @return array the values for the entity.
        */
        public function getArrayColumn($array, $column)
        {
            $result = array();
            if (!is_array($array) || empty($column)) {
                return $result;
            } else {
                foreach ($array as $value) {
                    $result[] = $value->$column;
                }
                return $result;
            }
        }

        /**
        * Return the calculated price for single product.
        * If price type is % , calculate the discounted amount of price.
        * If price type is flat , return the flat price.
        * @param int $quantity Min Quantity.
        * @param array $priceArray Pricing-quantity array
        * @param int/float $regular_price regular price of product.
        * @return int/float Specific Price for the Product
        */
        public function priceForQuantity($quantity, $priceArray, $regular_price)
        {
            if (count($priceArray) == 0) {
                return false;
            }

            foreach ($priceArray as $a) {
                if ($a->min_qty == $quantity) {
                    if ($a->price_type == 2) {
                        return ($regular_price) - (round(($a->price * $regular_price), wc_get_price_decimals()) / 100);
                    }
                    return $a->price;
                }
            }
        }

        /**
        * Gets the Specific pricing for that product for that quantity.
        * Returns the array for the Specific price for the quantity of that
        * product.
        * @param int $quantity quantity of product.
        * @param array $priceArray Quantity pricing array
        * @param object $product Product object.
        * @return array Specific Price on quantity.
        */
        public function priceForSearchQuantity($quantity, $priceArray, $product)
        {
            $regularPrice = floatval(get_post_meta($product->get_id(), '_regular_price', true));
            if (count($priceArray) == 0) {
                return false;
            }

            foreach ($priceArray as $a) {
                if ($a->min_qty == $quantity) {
                    return $this->getCSPArray($a, $a->price, $product);
                }
            }
        }

        /**
        * Returns the array for the Specific price for the quantity of that
        * product.
        * @param object $a pricing quantity based.
        * @param float $price Price for the selected quantity.
        * @param object $product Product Object.
        * @return array $cspPrice Specific Price on quantity.
        */
        public function getCSPArray($a, $price, $product)
        {
            $cspPrice = array();
            $cspPrice['price'] = $price;
            $cspPrice['min_qty'] = $a->min_qty;
            $cspPrice['price_type'] = $a->price_type;
            $cspPrice['product_id'] = $product->get_id();
            if (property_exists($a, 'cat_slug')) {
                $cspPrice['price_set'] = $a->price_set;
                $cspPrice['source'] = $a->cat_slug;
            } else {
                $cspPrice['source'] = 'Direct';
            }
            return $cspPrice;
        }

        /**
        * Returns the  attachments, revisions, or sub-pages, possibly by
        * product
        * @param object $product Product to get variations of.
        * @return object details associated with the product.
        */
        public function getVariationId($product)
        {
            return $product->get_children();
        }

        /**
        * Returns the HTML for error display.
        * @param string $message  message.
        * @param int $rule_id Rule Id.
        * @param bool $error true if some error.
        * @return string HTML for error.
        */
        private function selectionListMessage($message, $rule_id, $error = true)
        {
            global $wpdb, $subruleManager, $cspFunctions, $ruleManager;
            $divClass = 'updated';
            $option_selected     = '';
            $query_log_result    = '';
            
            $csp_ajax = new \cspAjax\WdmWuspAjax();

            if (! empty($rule_id)) {
                //$query = 'SELECT `selection_type`, `selection_list`, `product_list`, `query_title` FROM `'.$wpdb->prefix.'wusp_query_log` WHERE `query_id` = '.$query_log_id;

                $query               = "SELECT rule_title, rule_type FROM {$ruleManager->ruleTable} WHERE rule_id = %d";
                $query_log_result    = $wpdb->get_row($wpdb->prepare($query, $rule_id));
                if ($query_log_result != null) {
                    $option_selected = strtolower($query_log_result->rule_type);
                }
            }

            if ($error) {
                $divClass = 'error';
            }

            $productResultObject = $this->getProductResultArray($option_selected, $query_log_result, $rule_id, $csp_ajax);
            $update_div = '<div rule_id="' . $rule_id . '" class="' . $divClass . ' wdm_result"><p>' . $message . '</p></div>';
            $updateResult = array(
                'product_result' => $productResultObject['product_result'],
                'update_div' => $update_div
            );

            return $updateResult;
        }

        /**
        * Get the display names form the associated entities table of subrule.
        * User-names, Roles, Group-names.
        * @param string $option_selected rule type selected.
        * @param array $selectedEntities Associated entities array for the
        * subrules of that rule.
        * @return array $selectionValues array of display names.
        */
        public function getSelectionValues($option_selected, $selectedEntities)
        {
            global $wpdb;
            $selectionValues = array();

            if ($option_selected == "customer") {
                foreach ($selectedEntities as $value) {
                    $result =  $wpdb->get_row($wpdb->prepare("SELECT display_name FROM ".$wpdb->prefix."users WHERE ID = %d", $value));
                    $selectionValues[$value] = $result->display_name;
                }
            } elseif ($option_selected == "role") {
                $availableRoles = array_reverse(get_editable_roles());
                $roleKeys = array();
                if (! empty($availableRoles)) {
                    $roleKeys = array_keys($availableRoles);
                }
                foreach ($selectedEntities as $value) {
                    if (in_array($value, $roleKeys)) {
                        $selectionValues[$value] = translate_user_role($availableRoles[$value]['name']);
                    }
                }
            } elseif ($option_selected == "group") {
                foreach ($selectedEntities as $value) {
                    $result =  $wpdb->get_row($wpdb->prepare("SELECT name FROM ".$wpdb->prefix."groups_group WHERE group_id = %d", $value));
                    $selectionValues[$value] = $result->name;
                }
            }
            return $selectionValues;
        }

        /**
        * Gets the subrules for the Rules.
        * For Product-Pricing tab,
        * Display the Products and Rule-type selection.
        * Display the Set Prices module.
        * Gets the Product details for the rule selection.
        * Get the Rules page template
        * @param string $option_selected rule type selected.
        * @param array $query_log_result Rows for the rule_id.
        * @param int $query_log_id rule-id
        * @return array $product_result array of Products titles, details,
        * rules info.
        */
        public function loadExistingDetails($option_selected, $query_log_result, $query_log_id)
        {
            $productResultObject = "";
            $csp_ajax = new \cspAjax\WdmWuspAjax();
            if (! empty($option_selected) && ! is_wp_error($query_log_result)) {
                $productResultObject = $this->getProductResultArray($option_selected, $query_log_result, $query_log_id, $csp_ajax);
                $csp_ajax->displayTypeSelection($option_selected, $productResultObject['selectedEntities'], $productResultObject['selectedProducts']);
            }

            return !empty($productResultObject) ? $productResultObject['product_result'] : $productResultObject;
        }

        /**
        * Gets the subrules for the Rules.
        * Get the associated-entities for the subrules.
        * For Product-Pricing tab,
        * Gets the Product Details titles
        * Gets the product names , product variation names.
        * Gets the Product details for the rule selection.
        * Get the Rules page template
        * @param string $option_selected rule type selected.
        * @param array $query_log_result Rows for the rule_id.
        * @param int $query_log_id rule-id
        * @return array $productResultObject array of Products titles, Seleccted entities and Selected produts.
        */
        public function getProductResultArray($option_selected, $query_log_result, $query_log_id, $csp_ajax)
        {
            global $subruleManager;
            $product_result      = array();
            $subruleInfo         = array();
            $selectedEntities    = array();
            $selectedProducts    = array();
            $selectionValues     = array();
            $productResultObject = array();

            if ($query_log_id != null) {
                $subruleInfo = $subruleManager->getAllSubrulesInfoForRule($query_log_id);
            }
                    
            if (empty($subruleInfo)) {
                return;
            }

            //Find out all entities and products which were selected
            if (is_array($subruleInfo)) {
                foreach ($subruleInfo as $singleSubrule) {
                    if (! in_array($singleSubrule[ 'associated_entity' ], $selectedEntities)) {
                        $selectedEntities[] = $singleSubrule[ 'associated_entity' ];
                    }

                    $selectionValues = self::getSelectionValues($option_selected, $selectedEntities);

                            
                    if (! in_array($singleSubrule[ 'product_id' ], $selectedProducts)) {
                        $selectedProducts[] = $singleSubrule[ 'product_id' ];
                    }
                }
            }          
            // var_dump($product_result[ 'title_name' ]);exit;
            $product_result[ 'title_name' ] = $csp_ajax->getProductDetailTitles($option_selected);
            $product_name_list   = array();
            $product_list        =  $selectedProducts;

            $product_name_list = $this->getProductNameList($product_list);

            $product_result[ 'value' ] = $csp_ajax->getProductDetailList($option_selected, $product_name_list, $selectionValues, $subruleInfo);

            $product_result[ 'query_input' ] = $csp_ajax->getQueryInput($query_log_result->rule_title);
            $productResultObject = array(
                'selectedEntities' => $selectedEntities, 
                'selectedProducts' => $selectedProducts, 
                'product_result' => $product_result, 
            );

            return $productResultObject;
        }

        /**
        * Gets the Product Details titles
        * Gets the product names , product variation names.
        * Gets the Product details for the rule selection.
        * @return array $product_name_list array of Products titles with all attributes for variations(if available).
        */
        public function getProductNameList($product_list)
        {
            $product_name_list = array();
            foreach ($product_list as $single_product_id) {
                if (get_post_type($single_product_id) == 'product_variation') {
                    $parent_id           = wp_get_post_parent_id($single_product_id);
                    $product_title       = get_the_title($parent_id);
                    $variable_product    = new \WC_Product_Variation($single_product_id);
                    $attributes          = $variable_product->get_variation_attributes();

                    //get all attributes name associated with this variation
                    $attribute_names = array_keys($variable_product->get_attributes());

                    $pos = 0; //Counter for the position of empty attribute
                    foreach ($attributes as $key => $value) {
                        if (empty($value)) {
                            $attributes[$key] = "Any ".$attribute_names[$pos++];
                        }
                    }

                    $product_title .= '-->' . implode(', ', $attributes);

                    $product_name_list[ $single_product_id ] = $product_title;
                } else {
                    $product_name_list[ $single_product_id ] = get_the_title($single_product_id);
                }
            }

            return $product_name_list;
        }

        /**
        * Displays the message for the Rules page.
        * @param bool $ruleCreated true if rule created else false.
        * @param string $message error messages from rule and subrules manager.
        * @param array $selection_entry_list insertion ids of pricing mappings.
        * @param int $rule_id Rule Id.
        * @param int $current_query_id rule id.
        */
        private function sendSelectionResult($ruleCreated, $message, $selection_entry_list, $rule_id, $current_query_id = null)
        {

            if (! empty($selection_entry_list)) {
                if (! $ruleCreated) {
                    return self::selectionListMessage($message, $rule_id);
                } else {
                    $message = trim($message);
                    if (empty($message)) {
                        if (empty($current_query_id)) {
                            $message = sprintf(__('Rule created successfully. Click %s here %s to add new rule.', CSP_TD), '<a href="admin.php?page=customer_specific_pricing_single_view&tabie=product_pricing">', '</a>');
                        } else {
                            $message = sprintf(__('Rule updated successfully. Click %s here %s to add new rule.', CSP_TD), '<a href="admin.php?page=customer_specific_pricing_single_view&tabie=product_pricing">', '</a>');
                        }
                    }

                    return self::selectionListMessage($message, $rule_id, false);
                }
            } else {
                return self::selectionListMessage(__('Values may be improper.', CSP_TD), $rule_id);
            }
        }

        /**
        * Update the Progress in options table.
        * @param string $option_name rule-type.
        * @param int $row_count intially zero
        * @param int $total_process_count product of count of selections and * products
        */
        private static function updateProgressOption($count, $total_rows, $option_name)
        {
            if ($count === 0) {
                update_option($option_name . "__value", '10');
                update_option($option_name . "_status", __('Initializing', CSP_TD));
            } elseif ($count === $total_rows) {
                update_option($option_name . "_value", '95');
            } elseif ($count > abs($total_rows * 0.75)) {
                update_option($option_name . "_value", '80');
            } elseif ($count > abs($total_rows * 0.5)) {
                update_option($option_name . "_value", '60');
            } elseif ($count > abs($total_rows * 0.4)) {
                update_option($option_name . "_value", '40');
            } elseif ($count > abs($total_rows * 0.2)) {
                update_option($option_name . "_value", '22');
            } else {
                update_option($option_name . "_value", '15');
            }
        }

        /**
        * If already the pricing pair doesn't exist add new pair in DB.
        * @param string $selection_type rule-type.
        * @param int $selection_id Selection id.
        * @param int $product_id Product Id.
        * @param float $price Price of Product.
        * @param int $flat_or_discount 2 for % and 1 for flat.
        * @param int $quantity Quantity for Product.
        * @return int $insert_id insertion id.
        */
        private static function setSelectionPair($selection_type, $selection_id, $product_id, $price, $flat_or_discount, $quantity)
        {
            //Insert the result
            $insert_id = -1;

            if ($selection_type === 'customer') {
                \WdmCSP\WdmWuspAddDataInDB::insertPricingInDb($selection_id, $product_id, $flat_or_discount, $price, $quantity);
            } elseif ($selection_type === 'role') {
                $insert_id = \WdmCSP\WdmWuspAddDataInDB::insertRoleProductMappingInDb($selection_id, $product_id, $flat_or_discount, $price, $quantity);
            } elseif ($selection_type === 'group') {
                $insert_id = \WdmCSP\WdmWuspAddDataInDB::insertGroupProductPricingInDb($selection_id, $product_id, $flat_or_discount, $price, $quantity);
            }

            return $insert_id;
        }

        /**
        * Updates the product pricing details in DB.
        * @param string $selection_type rule-type.
        * @param int $existing_id id for the product-pricing
        * @param int $selection_id Selection id.
        * @param int $product_id Product Id.
        * @param float $price Price of Product.
        * @param int $flat_or_discount 2 for % and 1 for flat.
        * @param int $quantity Quantity for Product.
        */
        private static function updateSelectionPair($selection_type, $existing_id, $selection_id, $product_id, $price, $flat_or_discount, $quantity)
        {
            if ($selection_type === 'customer') {
                \WdmCSP\WdmWuspUpdateDataInDB::updateUserPricingInDb($existing_id, $price, $flat_or_discount, $quantity);
            } elseif ($selection_type === 'role') {
                \WdmCSP\WdmWuspUpdateDataInDB::updateRolePricingInDb($existing_id, $selection_id, $product_id, $price, $flat_or_discount, $quantity);
            } elseif ($selection_type === 'group') {
                \WdmCSP\WdmWuspUpdateDataInDB::updateGroupPricingInDb($existing_id, $selection_id, $product_id, $price, $flat_or_discount, $quantity);
            }
        }

        public function searchAllOccurrences($arr, $needle)
        {
            $array_keys = array();
            foreach ($arr as $key => $value) {
                if ($value == $needle) {
                    array_push($array_keys, $key);
                }
            }
            return $array_keys;
        }
        /**
        * Compares the product/category pricing mapping from admin side and entries stored in database.
        * Returns the entry which is not stored in database.
        * @param array $array1 array of pricing mapping present/set on admin side.
        * @param array $array2 array of pricing mapping in database previously.
        * @param string $userType user Type
        * @param bool true if category set if false check for product.
        * @return array $newArray new entry which is not in database.
        */
        public function multiArrayDiff($array1, $array2, $userType, $cat = false)
        {
            $res = false;
            $newArray = array();
            foreach ($array2 as $key => $val) {
                if ($cat) {
                    $res = self::compareArrayCategory($array1, $val, $userType);
                } else {
                    $res = self::compareArrayProduct($array1, $val, $userType);
                }
                if (!$res) {
                    $newArray[$key] = $val;
                }
            }
            return $newArray;
        }
        /**
        * Compare the array of product price mapping displayed on admin and stored 
        * in database are equal or not.
        * @param array $arr1 array of pricing mapping present/set on admin 
        * side.
        * @param array $arr2 array of pricing mapping in database previously.
        * @param string $userType user Type
        * @return bool true if equal, false if not.
        */
        public function compareArrayProduct($arr1, $arr2, $userType)
        {
            foreach ($arr1 as $val) {
                if ($val[$userType] == $arr2[$userType] && $val['min_qty'] == $arr2['min_qty']) {
                    return true;
                }
            }
            return false;
        }

        /**
        * Compare the array of category price mapping displayed on admin and stored
        * in database are equal or not.
        * @param array $arr1 array of pricing mapping present/set on admin
        * side.
        * @param array $arr2 array of pricing mapping in database previously.
        * @param string $userType user Type
        * @return bool true if equal, false if not.
        */
        public function compareArrayCategory($arr1, $arr2, $userType)
        {
            foreach ($arr1 as $val) {
                if ($val[$userType] == $arr2[$userType] && $val['min_qty'] == $arr2['min_qty'] && $val['cat_slug'] == $arr2['cat_slug']) {
                    return true;
                }
            }
            return false;
        }
    }
}
$GLOBALS['cspFunctions'] = WdmWuspFunctions::getInstance();
